#!/bin/bash
# set -euxo pipefail

function build-book {

  if [[ -z "$1" || -z "$2" || -z "$3" ]]; then
    echo "Usage: build-book REF DIR INVALIDATION_PATH"
    exit 1
  fi

  REF=$1
  DIR=$2
  INVALIDATION_PATH=$3


  # Checkout the ref we were given
  echo "before"
  git checkout -q master
  echo "mid"
  git checkout -q v0.15.0
  echo "after"
  git checkout -q master
  echo "reset"
  HEAD_REV=$(git rev-parse HEAD)


  # Check if the existing ref matches
  if [[ -f "$DIR" && -f "${DIR}/.rev" && "$(cat ${DIR}/.rev)" = "$HEAD_REV" ]]; then
    echo "Cached book build for $REF found in $DIR!"
    exit 0
  fi

  rm -rf $DIR
  mkdir -p $DIR
  echo "Building book for $REF..."
  mdbook build book -d "../$DIR"
  
  echo "after build"
  git checkout -q v0.15.0
  echo "last"
  # Write the newly built rev to the rev file
  echo "$HEAD_REV" >> $DIR/.rev

  # Write the invalidation path since we just rebuilt
  echo "$INVALIDATION_PATH" >> ./book-paths-updated

  ls examples/rendy

}

function build_docs_wasm {
  if [[ -z "$1" || -z "$2" || -z "$3" ]]; then
    echo "Usage: build-book REF DIR INVALIDATION_PATH"
    exit 1
  fi

  REF=$1
  DIR=$2
  INVALIDATION_PATH=$3
  
  # Checkout the ref we were given
  git checkout -q -f $REF
  HEAD_REV=$(git rev-parse HEAD)

  # Check if the existing ref matches
  if [[ -f "$DIR" && -f "${DIR}/.rev" && "$(cat ${DIR}/.rev)" = "$HEAD_REV" ]]; then
    echo "Cached docs build for $REF found in $DIR!"
    exit 0
  fi

  rm -rf $DIR
  mkdir -p $DIR
  echo "Building wasm docs for $REF..."

  # TODO: build wasm branch here
  # cargo doc --all --features="animation gltf vulkan"

  # taken from run.sh for future reference:
  # # (cd amethyst_animation && cargo doc --no-deps)
  # (cd amethyst_assets && cargo doc --target wasm32-unknown-unknown --features="wasm" --no-deps)
  # (cd amethyst_audio && cargo doc --target wasm32-unknown-unknown --no-default-features --features="wasm vorbis wav" --no-deps)
  # (cd amethyst_config && cargo doc --target wasm32-unknown-unknown --no-deps)
  # (cd amethyst_controls && cargo doc --target wasm32-unknown-unknown --features="wasm" --no-deps)
  # (cd amethyst_core && cargo doc --target wasm32-unknown-unknown --no-default-features --features="wasm" --no-deps)
  # (cd amethyst_derive && cargo doc --target wasm32-unknown-unknown --no-deps)
  # (cd amethyst_error && cargo doc --target wasm32-unknown-unknown --no-deps)
  # # (cd amethyst_gltf && cargo doc --no-deps)
  # # (cd amethyst_input && cargo doc --target wasm32-unknown-unknown --features="wasm" --no-deps)
  # # (cd amethyst_locale && cargo doc --no-deps)
  # (cd amethyst_network && cargo doc --target wasm32-unknown-unknown --features="web_socket" --no-deps)
  # (cd amethyst_rendy && cargo doc --target wasm32-unknown-unknown --features="wasm gl" --no-deps)
  # # (cd amethyst_test && cargo doc --features="wasm gl web_socket" --no-deps)
  # # (cd amethyst_tiles && cargo doc --features="wasm gl web_socket" --no-deps)
  # # (cd amethyst_ui && cargo doc --target wasm32-unknown-unknown --no-default-features --features="gl wasm" --no-deps)
  # # (cd amethyst_utils && cargo doc --target wasm32-unknown-unknown --no-default-features --features="wasm" --no-deps)
  # (cd amethyst_window && cargo doc --target wasm32-unknown-unknown --no-default-features --features="wasm" --no-deps)
  # cargo doc --target wasm32-unknown-unknown --no-default-features --features="audio network renderer wasm vorbis wav gl web_socket" --no-deps
  # cd ..

  # mv target/doc/* $DIR/

  # Write the newly built rev to the rev file
  # echo "$HEAD_REV" >> $DIR/.rev

  # Write the invalidation path since we just rebuilt
  # echo "$INVALIDATION_PATH" >> ./docs-paths-updated
}

function build-docs {
  echo "test"
  if [[ -z "$1" || -z "$2" || -z "$3" ]]; then
    echo "Usage: build-book REF DIR INVALIDATION_PATH"
    exit 1
  fi

  REF=$1
  DIR=$2
  INVALIDATION_PATH=$3

  # Checkout the ref we were given
  git checkout -q $REF
  HEAD_REV=$(git rev-parse HEAD)

  # Check if the existing ref matches
  if [[ -f "$DIR" && -f "${DIR}/.rev" && "$(cat ${DIR}/.rev)" = "$HEAD_REV" ]]; then
    echo "Cached docs build for $REF found in $DIR!"
    exit 0
  fi

  rm -rf $DIR
  mkdir -p $DIR
  echo "Building docs for $REF..."
  cargo doc --all --features="animation gltf vulkan"
  mv target/doc/* $DIR/

  # Write the newly built rev to the rev file
  echo "$HEAD_REV" >> $DIR/.rev

  # Write the invalidation path since we just rebuilt
  echo "$INVALIDATION_PATH" >> ./docs-paths-updated
}
